import os
from setuptools import setup, find_packages

# Utility function to read the README file.
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
        name = "torrent_reader",
        version = "1.0.0",
        author = "Saqib Yawar",
        author_email = "saqibyawar@gmail.com",
        description = ("A parser for torrents to read some information"),
        license = "BSD",
        keywords = "Torrent Parser Reader Tokenizer",
        url = "git@bitbucket.org:ssaqibyawargmailcom/treader.git",
        packages=find_packages(),
        long_description=read('README'),
        classifiers=[
            "Development Status :: 1 - Alpha",
            "Topic :: Utilities",
            "License :: OSI Approved :: BSD License",
        ],
    )