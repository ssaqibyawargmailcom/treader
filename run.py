import argparse
from torrent_reader.torrent_file import TorrentFile

__author__ = 'syawar'



def use_treader_module(filename):
    """
    Print all the possible torrent info for a particular torrent
    :param filename: the absolute path to the torrent file
    """
    torrent_file=TorrentFile(filename)
    if torrent_file:
        print "=====================TReader Torrent Information=============================="
        print "Filename\t\t"+filename
        print "Created By\t\t"+torrent_file.get_creation_client()
        print "Creation Date\t"+torrent_file.get_creation_date()
        print "Primary Tracker\t"+torrent_file.get_primary_tracker()

        print "\n"
        emebedded_files=torrent_file.get_all_embeded_file_info()
        print "Embedded Files: Total({0} files)".format(len(emebedded_files))
        for e_files in emebedded_files:
            print "\tFilename\t"+str(e_files["filename"])
            print "\t\tLength\t\t"+str(e_files["length"])
            print "\t\tChecKSum\t"+str(e_files["checksum"])+"\n"

        file_pieces= torrent_file.get_file_pieces_info()
        count=1
        print "Pieces: Total({0} pieces)".format(len(file_pieces))
        for p_files in file_pieces:
            print "\tSHA-1 hash of piece #"+str(count)+":\t"+p_files
            count+=1

        trackers = torrent_file.get_list_of_trackers()
        count =0
        print "Trackers: Total({0} trackers)".format(len(trackers))
        for track in trackers:
            print "\tTracker #"+str(count)+":\t"+track
            count+=1

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Torrent Reader')
    parser.add_argument('--filename', '-f',
        help='the absolute path to the torrent file to read' )

    args = parser.parse_args()

    if args.filename:
        use_treader_module(args.filename)