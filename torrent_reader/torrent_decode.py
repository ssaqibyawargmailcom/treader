"""
The code for this file was taken originally from

http://effbot.org/zone/bencode.htm

The code has been modified since to improve/customize the use
"""

__author__ = 'syawar'
import logging
import re
from common import set_logger

logger = logging.getLogger(__name__)
#logger=set_logger(logger, __name__)

def tokenize(text, match=re.compile("([idel])|(\d+):|(-?\d+)").match):
    """
    converts text to a type code followed by the string value
    :param text: the string to tokenise
    :param match: the regex to split the string
    """
    i = 0
    while i < len(text):
        m = match(text, i)
        s = m.group(m.lastindex)
        i = m.end()
        if m.lastindex == 2:
            yield "s"
            yield text[i:i+int(s)]
            i = i + int(s)
        else:
            yield s

def decode_item(next, token):
    """
    Converts the passed text into a proper python object using the token
    :param next: the value to decode
    :param token: the type d,l,s,e
    :return: value converted to appropriate python object
    """
    if token == "i":
        # integer: "i" value "e"
        data = int(next())
        if next() != "e":
            raise ValueError
    elif token == "s":
        # string: "s" value (virtual tokens)
        data = next()
    elif token == "l" or token == "d":
        # container: "l" (or "d") values "e"
        data = []
        tok = next()
        while tok != "e":
            data.append(decode_item(next, tok))
            tok = next()
        if token == "d":
            data = dict(zip(data[0::2], data[1::2]))
    else:
        logger.error("ERROR Decoding item::{0}::with token::{1}::".format(next, token))
        raise ValueError
    return data

def decode(text):

    """
    Converts the passed text into a proper python object using the token
    :param text: the text to decode
    :return: the decoded text
    :raise SyntaxError:
    """
    try:
        src = tokenize(text)
        data = decode_item(src.next, src.next())
        for token in src: # look for more tokens
            logger.error("Trailing Junk...")
    except (AttributeError, ValueError, StopIteration):
        logger.error("Syntax error in text::{0}::".format(text))
        raise SyntaxError("syntax error")
    return data