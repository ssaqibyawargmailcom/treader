__author__ = 'syawar'
import logging
import os
from constants import LOGGING_LOCATION, LOGGING_FILE, LOGGING_FORMAT, LOGGING_LEVEL
from datetime import datetime

def set_logger(logger, name):
    """
    Sets the configuration of the logger for all classes within the package
    :param logger: the logger for a particular module
    :param name: the name of the module
    :return: the configured logger with the proper level and file
    """
    log_file=os.path.abspath(os.path.join(LOGGING_LOCATION,name))
    # create a file handler
    handler = logging.FileHandler(log_file)
    handler.setLevel(LOGGING_LEVEL)
    #create logging format
    formatter=logging.Formatter(LOGGING_FORMAT)
    handler.setFormatter(formatter)
    #adding handler to logger
    logger.addHandler(handler)

    #return configured logger
    return logger

def convert_timestamp_to_datetime(timestamp):
    """
    convert passed timestamp to human readable format
    :param timestamp: the timestamp to convert
    :return: human readable formatted as '%Y-%m-%d %H:%M:%S'
    """
    converted_date=timestamp
    if timestamp:
        converted_date=datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')
    return converted_date

def byte_to_hex( byteStr ):
    """
    Convert a byte string to it's hex string representation .
    """
    return ''.join( [ "%02X " % ord( x ) for x in byteStr ] ).strip()