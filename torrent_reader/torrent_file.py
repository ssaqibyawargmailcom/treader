import StringIO
import chardet
import itertools
from torrent_reader.torrent_decode import decode

__author__ = 'syawar'
import os.path
import logging
from common import set_logger, convert_timestamp_to_datetime, byte_to_hex
import datetime

logger = logging.getLogger(__name__)
#logger=set_logger(logger, __name__)

class TorrentFile:

    def __init__(self, torrent_filename):
        """
        Constructor for the torrent file
        :param torrent_filename: the absolute path of the torrent file
        """
        #if file exists, initialize the variable

        if os.path.isfile(torrent_filename):
            self.torrent_filename=torrent_filename
            try:
                data = open(torrent_filename, "rb").read()
                self.torrent_data=decode(data)
            except IOError:
                logger.error("Error Openning file::{0}::".format(torrent_filename))
                raise IOError("Error Openning file::{0}::".format(torrent_filename))
        else:
            #raise error that file does not exist
            logger.error("File {0} does not exist.".format(torrent_filename))
            raise IOError("File {0} does not exist.".format(torrent_filename))

    def get_primary_tracker(self, custom_key="announce"):
        """
        Get the primary tracker url
        :param custom_key: the key to retriece the tracker
        :return: the primary tracker url
        """
        return self.torrent_data.get(custom_key)

    def get_creation_client(self, custom_key="created by"):
        """
        Get the client that created the torrent
        :param custom_key: the key required to get the client name
        :return: client that created the file
        """
        return self.torrent_data.get(custom_key)

    def get_creation_date(self, custom_key="creation date"):
        """
        Get the creation date of the file
        :param custom_key: the key required to retrieve the creation date
        :return: creation date
        """
        return convert_timestamp_to_datetime(self.torrent_data.get(custom_key))

    def get_all_embeded_file_info(self, custom_key="info"):
        """
        get attributes of all files in the torrent
        :param custom_key: the key to retrieve the file info
        :return: a list of dicts, each dict having the filename,length & checksum of a file within the torrent
        """
        file_info=[]
        all_info=self.torrent_data.get(custom_key)
        if 'files' in all_info:
            all_files=all_info.get("files")
            if all_files:
                for single_file in all_files:
                    single_file_info={
                        "length": str(single_file["length"]) + " bytes",
                    }
                    file_info.append(single_file_info)
                    #since md5sum and path are optional
                    if 'md5sum' in single_file:
                        single_file_info["checksum"]=single_file["md5sum"]
                    else:
                        single_file_info["checksum"]= "No CheckSum Found"
                    if 'path' in single_file:
                        single_file_info["filename"]=single_file["path"]
                    else:
                        single_file_info["filename"]= "No Filename Found"
        else:
            single_file_info={
                "filename": all_info["name"],
                "length":  str(all_info["length"]) + " bytes",
            }
            #since md5sum is optional
            if 'md5sum' in all_info:
                single_file_info["checksum"]=all_info["md5sum"]
            else:
                single_file_info["checksum"]= "No CheckSum Found"
            file_info.append(single_file_info)
        return file_info


    def get_file_pieces_info(self, custom_key="info", max_bytes_in_pieces=20):
        """
        Get the sha1 hash for each piece
        :param custom_key: the key to get file info
        :param max_bytes_in_pieces: max bytes per sha
        :return: a list of sha1 for each piece
        """
        piece_info=[]
        all_info=self.torrent_data.get(custom_key)
        all_hex = byte_to_hex(all_info["pieces"]).split(" ")
        piece=""
        count = 0
        for h in all_hex:
            piece+=h
            count+=1
            if count >= max_bytes_in_pieces:
                piece_info.append(piece)
                piece=""
                count=0
        return piece_info


    def get_list_of_trackers(self, custom_key="announce-list"):
        """
        List of trackers for the torrent
        :param custom_key: the key to retrieve the list
        :return: the list of trackers for the torrent
        """
        trackers=[]
        #since trackets are optional
        if custom_key in self.torrent_data:
            all_trackers=self.torrent_data.get(custom_key)
            trackers = list(itertools.chain(*all_trackers))
        return trackers

