import os
import logging
__author__ = 'syawar'


#==================================================
#Logging config
#==================================================
LOGGING_LOCATION=os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'logs'))
LOGGING_FORMAT='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGING_LEVEL=logging.INFO
LOGGING_FILE=os.path.abspath(os.path.join(LOGGING_LOCATION,''))