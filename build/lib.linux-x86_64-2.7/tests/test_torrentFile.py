import os
from unittest import TestCase
from torrent_reader.torrent_file import TorrentFile

__author__ = 'syawar'


class TestTorrentFile(TestCase):

    TEST_FILE=os.path.dirname(os.path.realpath(__file__))+"/test_files/t5.torrent"
    test_torrent=TorrentFile(TEST_FILE)

    def test_get_primary_tracker(self):
        self.assertEqual("udp://tracker.publicbt.com:80/announce",self.test_torrent.get_primary_tracker())

    def test_get_creation_client(self):
        self.assertEqual("x_files2001", self.test_torrent.get_creation_client())

    def test_get_creation_date(self):
        self.assertEqual("2013-01-04 18:47:33", self.test_torrent.get_creation_date())

    def test_get_all_embeded_file_info(self):
        self.assertTrue(len(self.test_torrent.get_all_embeded_file_info())==234)

    def test_get_file_pieces_info(self):
        self.assertTrue(len(self.test_torrent.get_file_pieces_info())==14510)

    def test_get_list_of_trackers(self):
        self.assertTrue(len(self.test_torrent.get_list_of_trackers())==5)