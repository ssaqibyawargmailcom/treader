from unittest import TestCase
from torrent_reader.torrent_decode import tokenize, decode_item, decode

__author__ = 'syawar'


class TestTokenize(TestCase):

    def test_tokenize(self):
        expected=['s', 'spam']
        result=list(tokenize("4:spam"))
        self.assertIsNotNone(result)
        self.assertEqual(expected, result)

    def test_decode_item(self):
        text="i3e"
        expected = 3
        src = tokenize(text)
        result = decode_item(src.next, src.next())
        self.assertIsNotNone(result)
        self.assertEqual(result, expected)

    def test_decode(self):
        text="l4:spam4:eggse"
        expected=['spam', 'eggs']
        result=decode(text)
        self.assertEqual(result, expected)

        fail_text="l4:spam4:eggsef"
        self.assertRaises(SyntaxError, decode, fail_text)